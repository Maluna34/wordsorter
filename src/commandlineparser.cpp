#include "commandlineparser.h"

#include <cassert>
#include <stdexcept>

CommandLineParser::CommandLineParser(int argc, char *argv[])
    : m_sortType(SortType::Ascending), m_removeDuplicates(false)
{
    assert((argc > 0) && "Missing the exe file path in program args");

    for (int i { 1 }; i < argc; ++i)
        m_args.emplace_back(argv[i]);
}

std::string CommandLineParser::getInputFilePath() const
{
    return m_inputFilePath;
}

CommandLineParser::SortType CommandLineParser::getSortType() const
{
    return m_sortType;
}

bool CommandLineParser::removeDuplicates() const
{
    return m_removeDuplicates;
}

void CommandLineParser::parse()
{
    if (m_args.empty())
        throw std::invalid_argument("Missing input file path: <exe> <input file path> [-a|--asc|-d|--desc] [--rm-duplicates]");

    m_inputFilePath = m_args.front();

    for (size_t i { 1 }; i < m_args.size(); ++i)
    {
        const auto& arg { m_args.at(i) };

        if (arg == "-a" || arg == "--asc")
            m_sortType = SortType::Ascending;
        else if (arg == "-d" || arg == "--desc")
            m_sortType = SortType::Descending;
        else if (arg == "--rm-duplicates")
            m_removeDuplicates = true;
        else
            throw std::invalid_argument("Unknown argument " + arg);
    }
}
