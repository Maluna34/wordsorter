#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include <string>
#include <vector>

class CommandLineParser
{
public:

    enum class SortType
    {
        Ascending,
        Descending
    };

private:

    std::vector<std::string> m_args;

    std::string m_inputFilePath;

    SortType m_sortType;

    bool m_removeDuplicates;

public:

    CommandLineParser(int argc, char *argv[]);

    /**
     * \brief Return the input file path from program arguments.
     *
     * \return Input file path
     */
    std::string getInputFilePath() const;

    /**
     * \brief Return the sort type (ascending/descending) from program arguments.
     *
     * \return Sort type
     */
    SortType getSortType() const;

    /**
     * \brief Return true if removal of duplicates is enabled.
     *
     * \return True if duplicates are removed, false otherwise
     */
    bool removeDuplicates() const;

    /**
     * \brief Parse the command line arguments.
     * Throw an exception if arguments are missing or don't exist.
     */
    void parse();
};

#endif // COMMANDLINEPARSER_H
