#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <algorithm>

#include "commandlineparser.h"

std::vector<std::string> readWords(const std::string& inputFilePath)
{
    std::ifstream inputFile(inputFilePath, std::ios::in);
    if (!inputFile.is_open())
    {
        std::cerr << "Error when trying to open the file " << inputFilePath << std::endl;
        return {};
    }

    std::vector<std::string> words;

    std::string line;
    while (std::getline(inputFile, line))
    {
        if (!line.empty())
            words.push_back(line);
    }

    return words;
}

template<typename Comp>
std::set<std::string, Comp> toSet(const std::vector<std::string>& words)
{
    const std::set<std::string, Comp> set(words.cbegin(), words.cend());
    return set;
}

template<typename Container>
void displayWords(const Container& words)
{
    std::ofstream out("out.txt", std::ios::out);
    if (!out.is_open())
    {
        std::cerr << "Error when opening the output file" << std::endl;
        return;
    }

    for (const auto& word : words)
        out << word << '\n';
}

int main(int argc, char *argv[])
{
    CommandLineParser parser(argc, argv);

    try
    {
        parser.parse();
    }
    catch (const std::exception& e)
    {
        std::cerr << "Error when processing args: " << e.what() << std::endl;
        return -1;
    }

    auto words { readWords(parser.getInputFilePath()) };

    if (parser.removeDuplicates())
    {
        if (parser.getSortType() == CommandLineParser::SortType::Ascending)
            displayWords(toSet<std::less<std::string>>(words));
        else
            displayWords(toSet<std::greater<std::string>>(words));
    }
    else
    {
        if (parser.getSortType() == CommandLineParser::SortType::Ascending)
            std::sort(words.begin(), words.end(), std::less<std::string>());
        else
            std::sort(words.begin(), words.end(), std::greater<std::string>());

        displayWords(words);
    }
}
