# WordSorter

## Build

```
cmake <path>/src
make
```

## Run

```
<exe> <input file path> [-a|--asc|-d|--desc] [--rm-duplicates]


-a, --asc           Ascending sort
-d, --desc          Descending sort
--rm-duplicates     Enable removal of duplicates
```
